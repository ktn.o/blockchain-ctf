FROM ubuntu:latest
MAINTAINER Kittinan
ENV DEBIAN_FRONTEND noninteractive
RUN echo 'debconf debconf/frontend select Noninteractive' | debconf-set-selections
RUN apt-get update && apt-get install -y sudo && \
 apt-get install -y --no-install-recommends apt-utils dialog && \
 apt-get install -y curl make gcc build-essential software-properties-common && \
 curl -sL https://deb.nodesource.com/setup_11.x | sudo -E bash - && \
 sudo apt-get install -y nodejs && \
 sudo npm i -g truffle --unsafe-perm=true && \
 truffle init && \
 sudo apt-get install -y wget && \
 wget http://regsys.ml/blockchain-ctf/CTF.sol && \
 mv CTF.sol contracts && \
 truffle compile && \
 wget http://regsys.ml/blockchain-ctf/2_deploy_contracts.js.remove_this && \
 mv 2_deploy_contracts.js.remove_this migrations/2_deploy_contracts.js && \
 sudo rm -rf truffle-config.js && \
 wget http://regsys.ml/blockchain-ctf/truffle.js.remove_this && \
 mv truffle.js.remove_this truffle.js && \
 truffle migrate --network ctf && \
 sudo rm -rvf contracts migrations
CMD  ["executable", "param1", "param2", "param3"]

